# RSStream

[![CI Status](https://img.shields.io/travis/terrordrummer/RSStream.svg?style=flat)](https://travis-ci.org/terrordrummer/RSStream)
[![Version](https://img.shields.io/cocoapods/v/RSStream.svg?style=flat)](https://cocoapods.org/pods/RSStream)
[![License](https://img.shields.io/cocoapods/l/RSStream.svg?style=flat)](https://cocoapods.org/pods/RSStream)
[![Platform](https://img.shields.io/cocoapods/p/RSStream.svg?style=flat)](https://cocoapods.org/pods/RSStream)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSStream is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RSStream'
```

## Author

terrordrummer, roberto.sartori@gmail.com

## License

RSStream is available under the MIT license. See the LICENSE file for more info.
